#apk add --no-cache  nginx nginx-mod-http-echo bash iproute2  openssl      #varnish ;
apk upgrade;
apk add --no-cache  bash apache2-utils iproute2  openssl      #varnish ;
# OH NO alpine-linux
## module "/etc/nginx/modules/ngx_http_echo_module.so" version 1020001 instead of 1021003 in /etc/nginx/modules/10_http_echo.conf:1
#bash /0_crt-snakeoil.sh


##apk add --no-cache  git ;go get github.com/vektra/templar/cmd/templar
echo > /etc/nginx/nginx.conf &>/dev/null &


[[ -z ${UPSTREAM_HOST}  ]] && UPSTREAM_HOST=dnnd.de;
[[ -z ${UPSTREAM_FORCED_HOSTNAME} ]] && UPSTREAM_FORCED_HOSTNAME="${UPSTREAM_HOST}"
[[ -z ${CACHED_PROTO}   ]] &&  CACHED_PROTO=https;
[[ -z ${VIRTUAL_HOST}   ]] &&  VIRTUAL_HOST=nginx-cache-proxy.lan;
[[ -z ${EXPIREHEADER}   ]] && EXPIREHEADER=1m;


[[ ! -z "${AUTHSTRING}"  ]] && {
  htpasswd -c -b -B /dev/shm/htpasswd ${AUTHSTRING/:*/} ${AUTHSTRING/*:/}
}

mkdir -p /dev/shm/nginx-{static,backup}-cache /run/nginx/
echo "#############+++init nginx cache+++#########"
( echo '
pid        /var/run/nginx.pid;
# Includes files with directives to load dynamic modules.
include /etc/nginx/modules/*.conf;
worker_processes  '$(($(nproc)*2))';
events {
    worker_connections        1024;
}
http {
client_max_body_size 128M;
proxy_headers_hash_max_size 1024;
proxy_headers_hash_bucket_size 512;
map $http_xcachegetrequest $xcache {
    default   $http_xcachegetrequest;
    ""        "$host";
}
map $http_cf_connecting_ip $cfip {
    default   $http_cf_connecting_ip;
    ""        "127.0.0.1";
}
    include /etc/nginx/mime.types; # This includes the built in mime types
    include /logformats.conf;
    server {
      proxy_busy_buffers_size   512k;
      proxy_buffers   4 512k;
      proxy_buffer_size   256k;
      
      listen 80 ;
      server_name _ ;
      location /nginx_status         { stub_status; access_log off; allow 127.0.0.1; deny all ; }'
      [[ ! -z "${REPLACESTRING}"  ]] && {
      echo '
                  gunzip on;
                  sub_filter_once off;
                  sub_filter_types text/html text/css application/javascript text/xml;'
      }
#echo '
#    location @handle_redirects {
#        #set $saved_redirect_location '"'"'$upstream_http_location'"'"';
#        #proxy_pass $saved_redirect_location;
#        proxy_redirect '${CACHED_PROTO}'://'${UPSTREAM_HOST}'/    https://'${UPSTREAM_FORCED_HOSTNAME}'/;
#        proxy_pass     '${CACHED_PROTO}'://'${UPSTREAM_HOST}' ;
#
#        
#    }
#'
CURRENT_PATH=/
 {      echo 'location '${CURRENT_PATH}' {
            set_real_ip_from  10.0.0.0/8     ;
            set_real_ip_from  192.168.0.0/16 ;
            set_real_ip_from  172.16.0.0/12  ;
            set_real_ip_from  fe80::/64      ;
            set_real_ip_from  fc00::/7       ; # RFC 4193 Unique Local Addresses (ULA)
            real_ip_header    X-Forwarded-For;
            real_ip_recursive on;
            keepalive_timeout 10m;
            proxy_connect_timeout  10s;
            proxy_send_timeout  300s;
            proxy_read_timeout  300s;'
[[ ! "${KEEP_HOST}" = "true" ]] &&  { echo '            proxy_set_header       Host '${UPSTREAM_FORCED_HOSTNAME}' ;' ; } ;
[[ ! "${KEEP_HOST}" = "true" ]] ||  { echo '            proxy_set_header       Host '${VIRTUAL_HOST}' ;' ; } ; 

echo '
            proxy_set_header       Xcachegetrequest "$xcache";
            proxy_pass             '${CACHED_PROTO}'://'${UPSTREAM_HOST}' ;
            proxy_hide_header       Cookie;
#            proxy_ignore_headers    Cookie;
            proxy_ssl_verify              off;
#            proxy_hide_header       Set-Cookie;
#            proxy_ignore_headers    Set-Cookie;
#            proxy_pass             http://127.0.0.1:1234 ; ## varnish
#            proxy_pass             '${CACHED_PROTO}'://'${UPSTREAM_HOST}' ;
#            proxy_cache            STATIC;
#            proxy_cache_valid      200  '${CACHETIME}';
#            expires '${EXPIREHEADER}';
#            proxy_cache_use_stale  error http_502 http_503 http_504 timeout ;
#            proxy_set_header       X-Templar-Cache 'fallback' ;
#            proxy_set_header       X-Templar-CacheFor '15m' ;
            proxy_buffering        off;
            error_log              /dev/stderr ;'
[[ ! -z "${AUTHSTRING}" ]] && echo ' auth_basic "closed site"; auth_basic_user_file /dev/shm/htpasswd ;'
[[ "${ACCESS_LOG}" = "true" ]] &&  echo ' access_log             /dev/stdout upstream;' ;
[[ "${ACCESS_LOG}" = "true" ]] ||  echo ' access_log             off;' ;


[[ "${HIDECLIENT}" = "true" ]] ||  echo '
            proxy_set_header       CF-Connecting-IP "$cfip";
            proxy_set_header       X-Forwarded-For  "$cfip";' ;
[[ "${HIDECLIENT}" = "true" ]] &&  echo '
            proxy_set_header        "User-Agent" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:92.0) Gecko/20100101 Firefox/91.0";
            proxy_set_header       CF-Connecting-IP "10.254.254.254";
            proxy_set_header       X-Forwarded-For  "10.254.254.254";
            proxy_set_header       X-Real-IP        "10.254.254.254";
            proxy_set_header       cfip             "10.254.254.254";';

[[ ! -z "${REPLACESTRING}"  ]] && {
echo '
            sub_filter_once off;
            sub_filter_types text/html text/css application/javascript text/xml;'
for CURRSTRING in $(echo $REPLACESTRING|sed 's/,/\n/g;s/^ //g;s/ $//g');do
SEARCH=${CURRSTRING/:*/}
NEWTXT=${CURRSTRING/*:/}
echo '
            proxy_set_header Accept-Encoding "";
            sub_filter "'$SEARCH'" "'$NEWTXT'";'
done
}

 echo  '     #proxy_cache_use_stale  error timeout invalid_header updating http_500 http_502 http_503 http_504;
#            proxy_cache_valid 500 502 503 504 14m;
#            proxy_cache_valid 500 502 503 504 14m;
             proxy_intercept_errors on;
#            error_page 500 502 503 504 404 @fallback;

       }
#      location @fallback {
#            access_log             /dev/stdout fallback;
#    keepalive_timeout 10m;
#    proxy_connect_timeout  2s;
#    proxy_send_timeout  5s;
#    proxy_read_timeout  6s;
#            proxy_hide_header Cookie;
##            stub_status;
#            access_log off;
##            proxy_pass             '${CACHED_PROTO}'://'${UPSTREAM_HOST}' ;
#            proxy_pass            http://'${UPSTREAM_HOST}' ;
#            error_log              /dev/stderr ;
#            proxy_set_header       Host '${UPSTREAM_HOST}' ;
#            proxy_buffering        on;
#            error_log              /dev/stderr ;
#            access_log             /dev/stdout fallback;
#            proxy_cache            STATIC;
#            proxy_cache_valid 200 302 15m;
##            proxy_cache_valid 500 502 503 504 14m;
#            proxy_cache_valid 301      1h;
#            proxy_cache_valid any      14m;
#            proxy_cache_use_stale  error timeout invalid_header updating  http_500 http_502 http_503 http_504;
##            proxy_cache_valid 500 502 503 504 14m;
            proxy_intercept_errors on;
            #error_page 301 302 307 = @handle_redirects;
            proxy_redirect '${CACHED_PROTO}'://'${UPSTREAM_FORCED_HOSTNAME}'/   /;

#      }
        ' ; } ;



##############

CURRENT_PATH=/typo3/
 {      echo 'location '${CURRENT_PATH}' {
            set_real_ip_from  10.0.0.0/8     ;
            set_real_ip_from  192.168.0.0/16 ;
            set_real_ip_from  172.16.0.0/12  ;
            set_real_ip_from  fe80::/64      ;
            set_real_ip_from  fc00::/7       ; # RFC 4193 Unique Local Addresses (ULA)
            real_ip_header    X-Forwarded-For;
            real_ip_recursive on;
            keepalive_timeout 10m;
            proxy_connect_timeout  10s;
            proxy_send_timeout  300s;
            proxy_read_timeout  300s;'
echo '            proxy_set_header       Host '${VIRTUAL_HOST}' ;' ;

echo '
            proxy_set_header       Xcachegetrequest "$xcache";
            proxy_pass             '${CACHED_PROTO}'://'${UPSTREAM_HOST}' ;
            proxy_hide_header       Cookie;
#            proxy_ignore_headers    Cookie;
            proxy_ssl_verify              off;
#            proxy_hide_header       Set-Cookie;
#            proxy_ignore_headers    Set-Cookie;
#            proxy_pass             http://127.0.0.1:1234 ; ## varnish
#            proxy_pass             '${CACHED_PROTO}'://'${UPSTREAM_HOST}' ;
#            proxy_cache            STATIC;
#            proxy_cache_valid      200  '${CACHETIME}';
#            expires '${EXPIREHEADER}';
#            proxy_cache_use_stale  error http_502 http_503 http_504 timeout ;
#            proxy_set_header       X-Templar-Cache 'fallback' ;
#            proxy_set_header       X-Templar-CacheFor '15m' ;
            proxy_buffering        off;
            error_log              /dev/stderr ;'
#[[ ! -z "${AUTHSTRING}" ]] && echo ' auth_basic "closed site"; auth_basic_user_file /dev/shm/htpasswd ;'
[[ "${ACCESS_LOG}" = "true" ]] &&  echo ' access_log             /dev/stdout upstream;' ;
[[ "${ACCESS_LOG}" = "true" ]] ||  echo ' access_log             off;' ;


[[ "${HIDECLIENT}" = "true" ]] ||  echo '
            proxy_set_header       CF-Connecting-IP "$cfip";
            proxy_set_header       X-Forwarded-For  "$cfip";' ;
[[ "${HIDECLIENT}" = "true" ]] &&  echo '
            proxy_set_header        "User-Agent" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:92.0) Gecko/20100101 Firefox/91.0";
            proxy_set_header       CF-Connecting-IP "10.254.254.254";
            proxy_set_header       X-Forwarded-For  "10.254.254.254";
            proxy_set_header       X-Real-IP        "10.254.254.254";
            proxy_set_header       cfip             "10.254.254.254";';

[[ ! -z "${REPLACESTRING}"  ]] && {
echo '
            sub_filter_once off;
            sub_filter_types text/html text/css application/javascript text/xml;'
for CURRSTRING in $(echo $REPLACESTRING|sed 's/,/\n/g;s/^ //g;s/ $//g');do
SEARCH=${CURRSTRING/:*/}
NEWTXT=${CURRSTRING/*:/}
echo '
            proxy_set_header Accept-Encoding "";
            sub_filter "'$SEARCH'" "'$NEWTXT'";'
done
}

 echo  '     #proxy_cache_use_stale  error timeout invalid_header updating http_500 http_502 http_503 http_504;
#            proxy_cache_valid 500 502 503 504 14m;
#            proxy_cache_valid 500 502 503 504 14m;
#             proxy_intercept_errors on;
#            error_page 500 502 503 504 404 @fallback;

       }

        ' ; } ;



#################
### below we close http and server section
echo '    }

}
 ' ) | tee /etc/nginx/nginx.conf |grep -v '^#'  |nl 2>&1  |sed 's/#.\+//g;'| grep -v "^$"|grep -e ';' -e '{' -e '}'
###  ^^ show config      with lines ^
#

#nginx -t  && nginx -g  'daemon off;'

sleep 0.2
#while (true);do varnishd -a :80 -f /etc/varnish/default.vcl -F;sleep 0.2;done &
while (true);do curl -s 127.0.0.1/nginx_status|sed 's/$/|/g'|tr -d '\n'|sed 's/^/STATS: /g';echo;sleep 3600;done &
while (true);do nginx -t  && nginx -g  'daemon off;' ;sleep 0.4;done
#wait
