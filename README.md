# docker-nginx-force-domain
### simple ngninx container pretending a domain for another container

## Use Cases
With apps that redirect you to a preset url or having "licenses" locked to domains,
upgrades/fixes in isolated environments might become a pain.
So use this one to send a "fake" Host header.


## Getting started
* attach the real container TO THE SAME DOCKER NETWORK ( with its container name) , but do not set "VIRTUAL_HOST"
* `git clone https://gitlab.com/the-foundation/docker-nginx-force-domain.git`
* create a .env file , see DOTenv.example
* docker-compose up
---

<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/">
<div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/docker-nginx-force-domain/README.md/logo.jpg" width="480" height="270"/></div></a>
